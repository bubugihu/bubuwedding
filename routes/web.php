<?php

use App\Customer;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    return view('page1');
})->name('index');
Route::get('/wedding', function(){
    $image = Image::all();
    $stt = 0;
    return view('page3',compact('image','stt'));
})->name('wedding');

Route::get('admin/customer/listCustomer', function(){
    $customer = Customer::all();
    return view('admin.customer.listCustomer', compact('customer'));
})->name('list');
Route::get('admin/image/listImage', function(){
    $image = Image::all();
    return view('admin.image.listImage', compact('image'));
})->name('image');
Route::get('/deleteCustomer/{id}', function($id){
    Customer::find($id)->delete();
    return back();
})->name('deleteCustomer');
Route::get('/deleteImage/{id}', function($id){
    Image::find($id)->delete();
    return back();
})->name('deleteImage');
Route::get('/ajaxStoreName', function(Request $request){
    try{
        $name = $request->get('name');
        $relation = $request->get('relation');
        $newCus = new Customer();
        $newCus->customer_name=$name;
        $newCus->relation=$relation;
        $newCus->save();
        return response()->json([
            'status'    => 'true',
        ],Response::HTTP_OK);
    }catch(\Throwable $th){
        return response()->json([
            'status'    => 'false',
            'message'   => $th->getMessage(),
        ],Response::HTTP_INTERNAL_SERVER_ERROR);
    }  
});
Route::post('/ajaxStoreWish', function(Request $request){
    try{    
        //
        $name = $request->get('wishName');
        $avatar = $request->file('wishAvatar');
        $content= $request->get('wishContent');
        $newWish = new Image();
        $newWish->name=$name;
        $newWish->content=$content;
        $newWish->save();
        
        $extension = strtolower($avatar->getClientOriginalExtension());
        $nameIMG = $newWish->id.'.'.$extension;
        $avatar->move("images/", $nameIMG);
        $newWish->image=$nameIMG;
        $newWish->save();
        // echo "<pre>";var_dump($extension);var_dump($nameIMG);var_dump(strtolower($avatar));die;
        return back();
    }catch(\Throwable $th){
       Log::error($th->getMessage());
    }  
})->name('putWish');


