@extends('admin.layout.layout')
@section('title', 'Customers List - Admin')
@section('content')
<div class="page-holder w-100 d-flex flex-wrap">
  <div class="container-fluid px-xl-5">
    <section class="py-5">
      <div class="row">
        <div class="col-lg-12 mb-5">
          <div class="card">
            <div class="card-header bg-dark text-white">
              <h6 class="text-uppercase mb-0">Customer List</h6>
            </div>
            <div class="card-body">
              <!-- List Customer -->
              <table class="table card-text text-center" id="dbtable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Fullname</th>
                    <th>Relation</th>
                    <th>Created_at</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($customer as $c)
                  <tr>
                    <th scope="row" class="align-middle">{{$c->id}}</th>
                    <td class="align-middle">{{$c->customer_name}}</td>
                    <td class="align-middle">{{$c->relation}}</td>
                    <td class="align-middle">{{$c->created_at}}</td>
                    <td class="align-middle"><a href="{{route('deleteCustomer',$c->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?');<i class="fa fa-trash"></i>DELETE</a></td>
                  </tr>
                  @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection