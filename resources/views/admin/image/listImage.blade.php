@extends('admin.layout.layout')
@section('title', 'Wish List - Admin')
@section('content')
<div class="page-holder w-100 d-flex flex-wrap">
  <div class="container-fluid px-xl-5">
    <section class="py-5">
      <div class="row">
        <div class="col-lg-12 mb-5">
          <div class="card">
            <div class="card-header bg-dark text-white">
              <h6 class="text-uppercase mb-0">Wish List</h6>
            </div>
            <div class="card-body">
              <!-- List wish -->
              <table class="table card-text text-center" id="dbtable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Image</th>
                    <th>Created_at</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($image as $c)
                  <tr>
                    <th scope="row" class="align-middle">{{$c->id}}</th>
                    <td class="align-middle">{{$c->name}}</td>
                    <td class="align-middle">{{$c->content}}</td>
                    <td class="align-middle"><img src="{{asset('../images/'.$c->image)}}" class="img-fluid rounded" width="50%" alt=""></td>
                    <td class="align-middle">{{$c->created_at}}</td>
                    <td class="align-middle"><a href="{{route('deleteImage',$c->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?');<i class="fa fa-trash"></i>DELETE</a></td>
                  </tr>
                  @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  @endsection