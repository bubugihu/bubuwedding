<!DOCTYPE html>

 <html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Bubu Wedding</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

 

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
	<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="fonts/icomoon.ttf">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href='css/bootstrap.css'>

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{asset('css/owl-carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
	
	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<style>
#cancel:hover 
{
  background: #bbb;
  box-shadow: 0 0 20px #222;
  opacity: 0;
}
#cancel 
{
  font-family: Helvetica, sans-serif;
  word-wrap: break;
  text-align: center;
  position: relative;
  z-index: 9999;
  cursor: pointer;
}
/*  */
ul.wish {
    list-style: none outside none;
    padding-left: 0;
    margin-bottom:0;
    background-color: black;
}
li.wish {
    display: block;
    float: left;
    margin-right: 0px;
    cursor:pointer;
}
img.wish {
    display: block;
    height: auto;
    max-width: 100%;
}
input.w3-border::placeholder{
	color: #F14E95!important;
}
input.w3-border::-webkit-input-placeholder { /* Chrome/Opera/Safari */
	color: #F14E95!important;
}
input.w3-border::-moz-placeholder { /* Firefox 19+ */
	color: #F14E95!important;
}
input.w3-border:-ms-input-placeholder { /* IE 10+ */
	color: #F14E95!important;
}
input.w3-border:-moz-placeholder { /* Firefox 18- */
	color: #F14E95!important;
}
@media screen and (max-width: 768px) {
	#fh5co-header{
	background-position: -170px -25px!important;
}
}

/*  */
</style>
    <script>

function music(){
	audio.play();
}
    </script>
	</head>
	<body onscroll="music()">
        <audio style='display: none;' id='audio' controls src={{asset('music/music1.mp3')}}></audio>
	
	<div id="page" ">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-xs-2">
					<div id="fh5co-logo"><a >Bubu Wedding</a></div>
				</div>
				<div class="col-xs-10 text-right menu-1">
					<ul>
						<li class="active"><a >Bubu's Family</a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(../images/img4.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>Hung &amp; Anh</h1>
							<h2>We Are Getting Married</h2>
							<div class="simply-countdown simply-countdown-one"></div>
							<!-- <p><a href="#" class="btn btn-default btn-sm">Save the date</a></p> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-couple">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h2>Hello!</h2>
					<h3>September 26th, 2020 Ho Chi Minh City</h3>
					<p>We invited you to celebrate our wedding</p>
				</div>
			</div>
			<div class="couple-wrap animate-box">
				<div class="couple-half">
					<div class="groom">
						<img src="{{asset('images/gr4.jpg')}}" alt="groom" class="img-responsive">
					</div>
					<div class="desc-groom">
						<h3>Bubu</h3>
						<p>I can't believe it.</p>
					</div>
				</div>
				<p class="heart text-center"><i class="fa fa-heart" aria-hidden="true"></i></p>
				<div class="couple-half">
					<div class="bride">
						<img src="{{asset('images/br10.jpg')}}" alt="groom" class="img-responsive">
					</div>
					<div class="desc-bride">
						<h3>Chacha</h3>
						<p>This is a difficult decision. I'm young and i wanna to go to travel.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-event" class="fh5co-bg" style="background-image:url(../images/img5.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>Our Special Events</span>
					<h2>Wedding Events</h2>
				</div>
			</div>
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-10 col-md-offset-1">
							<div class="col-md-6 col-sm-6 text-center">
								<div class="event-wrap animate-box">
									<h3>Main Ceremony</h3>
									<div class="event-col">
										<i class="fa fa-clock-o" aria-hidden="true"></i>
										<span>6:00 AM</span>
										<span>11:00 AM</span>
									</div>
									<div class="event-col">
										<i class="fa fa-calendar" aria-hidden="true"></i>
										<span>Friday 25</span>
										<span>September, 2020</span>
									</div>
									<p>Traditional Wedding Ceremony <br> 237/12 Phan Van Han Street, Binh Thanh District<br> HCM City</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 text-center">
								<div class="event-wrap animate-box">
									<h3>Wedding Party</h3>
									<div class="event-col">
										<i class="fa fa-clock-o"></i>
										<span>05:00 PM</span>
										<span>10:00 PM</span>
									</div>
									<div class="event-col">
										<i class="fa fa-calendar" aria-hidden="true"></i>
										<span>Saturday 26</span>
										<span>September, 2020</span>
									</div>
									<p>The Gala Royale Event Hall. <br> 63 Mac Dinh Chi Street, Da Kao Ward, District 1<br> HCM City</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-couple-story">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>We Love Each Other</span>
					<h2>Our Story</h2>
					<p>It is a so long story.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<ul class="timeline animate-box">
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url(../images/st10.jpg);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">First We Meet</h3>
									<span class="date">July 06, 2012</span>
								</div>
								<div class="timeline-body">
									<p>She ask me to borrow carton. Ya, I have given her.</p>
								</div>
							</div>
						</li>
						<li class="timeline-inverted animate-box">
							<div class="timeline-badge" style="background-image:url(../images/st12.jpg);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">First Date</h3>
									<span class="date">August 13, 2012</span>
								</div>
								<div class="timeline-body">
									<p>I don't think she remember it then i sure that.</p>
								</div>
							</div>
						</li>
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url(../images/st11.jpg);"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">In A Relationship</h3>
									<span class="date">September 25, 2020</span>
								</div>
								<div class="timeline-body">
									<p>Now, our relationship looks like grow up.</p>
								</div>
							</div>
						</li>
			    	</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-gallery" class="fh5co-section-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<span>Our Memories</span>
					<h2>Wedding Gallery</h2>
					<p>You want to see those, don't you ? Just waiting for event :D.</p>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<div class="col-md-12">
					<ul id="fh5co-gallery-list">
						
					<li class="one-third animate-box " data-animate-effect="fadeIn" style="background-image: url(../images/ab10.jpg);"> 
                            <a  class="color-1">
							<div class="case-studies-summary">
								{{-- <span>xx Photos</span>
								<h2>Two Famers</h2> --}}
							</div>
                        </a>
					</li>
					<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(../images/ab2.jpg); ">
						<a  class="color-2">
							<div class="case-studies-summary">
								{{-- <span>xx Photos</span>
								<h2>Timer starts now!</h2> --}}
							</div>
						</a>
					</li>


					<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(../images/ab11.jpg); ">
						<a  class="color-3">
							<div class="case-studies-summary">
								{{-- <span>xx Photos</span>
								<h2>Time to shopping</h2> --}}
							</div>
						</a>
					</li>
					<!-- <li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-4.jpg); ">
						<a href="#" class="color-4">
							<div class="case-studies-summary">
								<span>xx Photos</span>
								<h2>Company's Conference Room</h2>
							</div>
						</a>
					</li>

						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-5.jpg); ">
							<a href="#" class="color-3">
								<div class="case-studies-summary">
									<span>xx Photos</span>
									<h2>Useful baskets</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-6.jpg); ">
							<a href="#" class="color-4">
								<div class="case-studies-summary">
									<span>xx Photos</span>
									<h2>Skater man in the road</h2>
								</div>
							</a>
						</li>

						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-7.jpg); ">
							<a href="#" class="color-4">
								<div class="case-studies-summary">
									<span>xx Photos</span>
									<h2>Two Glas of Juice</h2>
								</div>
							</a>
						</li>

						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-8.jpg); "> 
							<a href="#" class="color-5">
								<div class="case-studies-summary">
									<span>xx Photos</span>
									<h2>Timer starts now!</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-9.jpg); ">
							<a href="#" class="color-6">
								<div class="case-studies-summary">
									<span>xx Photos</span>
									<h2>Beautiful sunset</h2>
								</div>
							</a>
						</li> -->
					</ul>		
				</div>
			</div>
		</div>
	</div>

	<!-- <div id="fh5co-counter" class="fh5co-bg fh5co-counter" style="background-image:url(images/img_bg_5.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="display-t">
					<div class="display-tc">
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-users"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Estimated Guest</span>

							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-user"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="1000" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">We Catter</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-calendar"></i>
								</span>
								<span class="counter js-counter" data-from="0" data-to="402" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Events Done</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 animate-box">
							<div class="feature-center">
								<span class="icon">
									<i class="icon-clock"></i>
								</span>

								<span class="counter js-counter" data-from="0" data-to="2345" data-speed="5000" data-refresh-interval="50">1</span>
								<span class="counter-label">Hours Spent</span>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
{{--  --}}
<div id="fh5co-started" class="fh5co-bg" style="background-image:url(../images/img3.jpg);">
	<div  class="overlay"></div>
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
				<h2>Are You Attending?</h2>
				<p>Please Fill-up the form to notify you that you're attending. Thanks.</p>
			</div>
		</div>
		<div  class="row animate-box">
			<div class="col-md-10 col-md-offset-1">
				{{-- <form id="form" class="form-inline"> --}}
					<div class="col-md-4 col-sm-4">
						<div class="form-group">
							<input id="customerName" name="abc" type="text" class="form-control"   placeholder="Name">
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="form-group">
							<select name="relation" class="form-control" id="relation">
								<option value="bubu" style="color: black">Friend with bubu</option>
								<option value="chacha" style="color: black">Friend with heo</option>
								<option value="bubuchacha" style="color: black">Friend with heo & bubu</option>
							</select>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						{{-- <input id="submit" class="btn btn-default btn-block" value="I am Attending." readonly> --}}
						<button type="button" id="submit" class="btn btn-default btn-block">I am Attending.</button>                            
					</div>
				{{-- </form> --}}
				{{-- Modal --}}
				<div class="col-md-12">
					<div id="myModal" class="modal" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
						  <div class="modal-content">
							<div class="modal-body">
							  <p id="inner" class="text-center"></p>
							</div>
							<div class="modal-footer" style="text-align: center">
							  <button id="myBtnClose" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							</div>
						  </div>
						</div>
					  </div>
				</div>
				{{--  --}}
			</div>
		</div>
		<div id="inCase" style="height: 50px;" class="row animate-box">
			<button id="cancel"  class="btn btn-default" >No, I'm busy.</button>
		</div>
	</div>
</div>
{{--  --}}

	<!-- <div id="fh5co-services" class="fh5co-section-gray">
		<div class="container">
			
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>We Offer Services</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-calendar"></i>
						</span>
						<div class="feature-copy">
							<h3>We Organized Events</h3>
							<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-image"></i>
						</span>
						<div class="feature-copy">
							<h3>Photoshoot</h3>
							<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-video"></i>
						</span>
						<div class="feature-copy">
							<h3>Video Editing</h3>
							<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
						</div>
					</div>

				</div>

				<div class="col-md-6 animate-box">
					<div class="fh5co-video fh5co-bg" style="background-image: url(images/img_bg_3.jpg); ">
						<a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo"><i class="icon-video2"></i></a>
						<div class="overlay"></div>
					</div>
				</div>
			</div>

			
		</div>
	</div> -->
	{{--  --}}
	<div id="fh5co-testimonial">
		<div class="container">
			<div class="row">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<span>Best Wishes</span>
						<h2>Friends Wishes</h2>
					</div>
				</div>
				<div class="row">
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/28.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Nguyễn Chí Thành</span>
										<blockquote>
											<p>Chúc anh Bu và Vợ anh luôn luôn hạnh phúc bên nhau nha !!!</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/29.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Quang Dang</span>
										<blockquote>
											<p>Chúc mừng hạnh phúc tân lang, tân nương! Cuộc sống mới chắc chắn là hạnh phúc “không thể chối từ”, chăm chỉ tận hưởng nha!</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/30.jpeg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Nhàn Nhàn</span>
										<blockquote>
											<p>おめでとうございます。末永くを幸せに。Chúc anh chị luôn hạnh phúc, luôn là thần tượng để em ganh tỵ nhaaaaaaa</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/31.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Minh Dung</span>
										<blockquote>
											<p>Mãi hạnh phúc nhé 2 anh chị 🥰🥰</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/33.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Avocado</span>
										<blockquote>
											<p>Một cặp trời sinh rồi nè. Chúc 2 bạn bước sang trang mới vẫn luôn hoà hợp và hạnh phúc nha!</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/32.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Béo nhỏ</span>
										<blockquote>
											<p>Chúc chế iu mãi luôn được yêu thương săn sóc cưng chiều, chúc anh chị luôn hạnh phúc vui vẻ, luôn nắm chặt tay nhau nhé. ❤❤❤</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/35.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Minh Tâm</span>
										<blockquote>
											<p>Chúc mừng bạn thoát khỏi kiếp FA. Từ nay bạn đã có một chốn để về, một bờ vai để tựa. Trăm năm hạnh phúc nhé!</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					{{--  --}}
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/36.jpg')}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>Ngọc Bích</span>
										<blockquote>
											<p>Chúc sư phụ Bubu và sư mẫu Cha Cha cùng nhau bắt đầu một hành trình mới cùng nhau thật vui vẻ và hạnh phúc.</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					{{--  --}}
					@foreach($image as $image)
					<div class="col-md-12 animate-box">
						<div class="wrap-testimony">
							{{-- <div class="owl-carousel"> --}}
								<div class="item">
									<div class="testimony-slide text-center">
										<figure>
											<img src="{{asset('../images/'.$image->image)}}" class="img-fluid" width= '100%' alt="user">
										</figure>
										<span>{{$image->name}}</span>
										<blockquote>
											<p>{{$image->content}}</p>
										</blockquote>
									</div>
								</div>
						</div>
					</div> 
					@endforeach
				</div>
			</div>
		</div>
	</div> 
	{{--  --}}
	<div id="fh5co-services" class="fh5co-section-gray container mb-5 " style="background-image:url(../images/img3146.jpg);background-size: cover;">
	<div class="row animate-box">
		<div  class="row animate-box">
			<div class="col-md-10 col-md-offset-1">
				<form id="formWish" method="post" action="{{route('putWish')}}" enctype="multipart/form-data">
					@csrf
					<div class="col-md-12 col-sm-12">
						<div class="form-group text-center fh5co-heading">
							<h2>Put Your Wish</h2>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<input id="wishName" name="wishName" type="text" class="form-control w3-border"  style="color: #F14E95"   placeholder="Your Name">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="wishAvatar" style="color: #F14E95">Put your Avatar Facebook</label>
								<input id="wishAvatar" name="wishAvatar" type="file" style="visibility:hidden;" class="form-control">
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<input id="wishContent" name="wishContent" type="text" class="form-control w3-border" style="color: #F14E95"   placeholder="Your Wish">
							</div>                 
						</div>
						<div class="col-md-12 col-sm-12 text-center">
							<button id="resetWish" type="button" class="btn rounded" style="background-color: #F14E95;color: #fff" >Reset</button>
							<button id="submitWish" type="button" class="btn rounded" style="background-color: #F14E95;color: #fff">Put</button>                 
						</div>
					</div>
					
					<div class="col-md-12">
                        <div id="myModalWish" class="modal" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
							  <div class="modal-content">
								<div class="modal-body">
								  <p id="innerWish" class="text-center"></p>
								</div>
								<div class="modal-footer" style="text-align: center">
								  <button  type="button" class="btn" style="background-color: #F14E95;color: #fff" data-dismiss="modal">Close</button>
								</div>
							  </div>
							</div>
						  </div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
	{{--  --}}

	

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container" style="height: 30px;">

			<div class="row copyright" >
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2020 Bubu Family. All Rights Reserved.</small> 
						<small class="block">Designed by <a style="color: #F14E95" >Bubu</a> Demo Images: <a style="color: #F14E95" >Bubu</a></small>
					</p>
				</div>
			</div>

		</div>
	</footer>
	

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
	<!-- Carousel -->
	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
	<!-- countTo -->
	<script src="{{asset('js/jquery.countTo.js')}}"></script>

	<!-- Stellar -->
	<script src="{{asset('js/jquery.stellar.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{asset('js/magnific-popup-options.js')}}"></script>

	<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="{{asset('js/simplyCountdown.js')}}"></script>
	<!-- Main -->
	<script src="{{asset('js/main.js')}}"></script>

	<script>
		$('#submit').click(function(){
			$('#myModal').modal('show');
			$(".modal-backdrop.in").hide();
			var name = $('#customerName').val();
			var relation =$('#relation').val();
			if(!name){
				$('#inner').html('Please enter your name');
			}else{
				$('#inner').html('Thank '+name+'. We will waitting you.');
				$.ajax({
            type: "get",
            url: '/ajaxStoreName',
            data: {
               name:name,relation:relation
            },
            dataType: "json",
            success: function (data) {
               console.log(data)
            },
            error: function (data) {
				console.log(data)
            }
        			});//end ajaxs
			}//end else
		});
		$('#resetWish').click(function(){
			$('#wishName').val('');
			$('#wishAvatar').val('');
			$('#wishContent').val('');
		});
				$('#wishAvatar').bind('change', function() {
            		alert('This Avatar size is: ' + this.files[0].size/1024/1024 + "MB");
        		});
		$('#submitWish').click(function(){
			let size = document.getElementById("wishAvatar").files[0].size/1024/1024;
			var wishName = $('#wishName').val();
			var wishAvatar = $('#wishAvatar').val();
			var wishContent = $('#wishContent').val();
			if(size >= 3){
				$('#myModalWish').modal('show');
				$(".modal-backdrop.in").hide();
				$('#innerWish').html('Avatar can not greater than 3mb');
			}else
			if(!wishName || !wishAvatar || !wishContent ){
				$('#myModalWish').modal('show');
				$(".modal-backdrop.in").hide();
				$('#innerWish').html('Please enter your information');
			}else{
				$('#formWish').submit();
			}//end else
		});
		//
		//
    var d = new Date(new Date("Sep 26, 2020 18:00:00").getTime() + 24 * 60 * 60 * 1000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: true
    });
    //music
    // function play() {
    //     var audio = document.getElementById("audio");
    //     audio.play();
    //     document.getElementById("front").style.display = "none";
    //   }
      //random
      $(document).ready(function(){
        $('#cancel').hover(function() {
    var bodyWidth = document.getElementById('inCase').clientWidth;
    var bodyHeight = document.getElementById('inCase').clientHeight;
    var randPosX = Math.floor((Math.random()*bodyWidth));
    var randPosY = Math.floor((Math.random()*bodyHeight));
    $('#cancel').css('left', randPosX);
    $('#cancel').css('top', randPosY);
    
});
});
</script>

	</body>
</html>

