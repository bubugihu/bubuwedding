<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
    .rand:hover {
    background: #bbb;
    box-shadow: 0 0 20px #222;
  }
  .rand {
    font-family: Helvetica, sans-serif;
    word-wrap: break;
    
    text-align: center;
    position: relative;
    z-index: 5;
    cursor: pointer;
  }
  
  
  
  p#timer {
    text-align: center;
    font-size: 40px;
    margin-top: 0px;
  }
  *:focus {
    outline: none;
  }
  #front {
      position: fixed;
      top: 0px;
      left: 0px;
      width: 100%;
      height: 100%;
      z-index: 11;
      /* background-color: #B4E1C8; */
      background-color: #fff;
      
  }
  #after {
      position: fixed;
      /* top: 0px;
      left: 0px; */
      width: 100%;
      height: 100%;
  
      /* background-color: #B4E1C8; */
      background-image: url('../img/bg.jpg');
      background-size: contain;
      background-repeat: no-repeat;
      background-position: center;
      
  }
  .after{
      position: relative;
      background-color: blanchedalmond;
  }
  @media only screen and (max-width: 580px) {
    /* For mobile phones: */
    .image-front{
        width: 100%;
        margin-top: 0!important;
    }
    
  }
  @media only screen and (max-width: 767px) {
    /* For mobile phones: */
    .flower{
        display: none;
    }
  }
  /* @media only screen and (min-width: 767px){
    .image-after{
        height: 50%;
    }
  } */
  #click {
      position: absolute;
      bottom: 50%;
      left: 50%;
      background-color: #B4E1C8;
      
  }
  .image-front{
      margin-top: 4%;
      position: relative;
  }
  .image-after{
      position: relative;
  }
</style>
</head>
<body >
    <div class="container">
       @yield('content')
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
</html>
