<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "wish";
    public $timestamps=true;
    // Declare primary key on table
    public $incrementing = true;
    protected $fillable = ['id', 'name', 'content','image','created_at'];
}
