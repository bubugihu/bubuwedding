<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customer";
    public $timestamps=true;
    // Declare primary key on table
    public $incrementing = true;
    protected $fillable = ['id', 'customer_name', 'relation','created_at'];
}
